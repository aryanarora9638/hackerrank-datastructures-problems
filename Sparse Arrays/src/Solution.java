import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the matchingStrings function below.
    static int[] matchingStrings(String[] strings, String[] queries) {
        int [] output = new int[queries.length];
        String queryItem = "";
        int repNumber;
        for( int item = 0; item < queries.length; item++){
            queryItem = queries[item];
            repNumber = 0;
            for (String stringsItem : strings){
                if(stringsItem.equals(queryItem)){
                    repNumber++;
                }
            }
            output[item] = repNumber;
        }
        return output;
    }

    static int[] matchingStringsWithCharSeq(String[] strings, String[] queries) {
        int [] output = new int[queries.length];
        String queryItem = "";
        int repNumber;
        for( int item = 0; item < queries.length; item++){
            queryItem = queries[item];
            repNumber = 0;
            for (String stringsItem : strings){
                if(stringsItem.contains(queryItem)){
                    repNumber++;
                }
            }
            output[item] = repNumber;
        }
        return output;
    }



    private static void printArray(String[] res) {
        System.out.print("[");
        for(String item : res){
            System.out.print(item + ",");
        }
        System.out.print("\b]");
        System.out.println();
    }


    public static void main(String[] args) throws IOException {


        String[] strings = new String[]{
                "aba", "baba", "aba", "xzxb"
        };
        printArray(strings);
        String[] queries = new String[]{
                "aba", "xzxb", "ab"
        };
       printArray(queries);
       int[] res = matchingStrings(strings, queries);
        System.out.print("Output rep : ");
       for(int item : res){
           System.out.print(item + ",");
       }
        System.out.print("\b");
    }


}
