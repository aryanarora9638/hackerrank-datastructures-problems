import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the hourglassSum function below.
    static int hourglassSum(int[][] arr) {
//        int maxSum = 0;
        int maxSum = Integer.MIN_VALUE;//to counter when the sum is a negative number
        for(int i = 0 ; i <= 3 ; i++){
            for(int j = 0 ; j <= 3; j++){
                int tempSum = singleHourGlass(i,j,arr);
                System.out.println("TempSum is : " + tempSum);
                if(tempSum >= maxSum){
                    maxSum = tempSum;
                }
            }
        }
        System.out.println("MaxSum is : " + maxSum);
        return maxSum;
    }

    static int singleHourGlass(int x , int y , int [][]arr){
        int tempSum = 0;
        for (int i = x; i < x+3; i++){
            for (int j = y; j < y+3; j++){
                if((i==x+1 && j==y) || (i==x+1 && j==y+2)){
                    System.out.print(" ");
                    continue;
                }
                else{
                    System.out.print(arr[i][j]);
                    tempSum += arr[i][j];
                }

            }
            System.out.println();
        }
        return tempSum;
    }


    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int[][] arr = new int[6][6];
        System.out.println("Array  is :");
        System.out.println();
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                arr[i][j] = (i+j);
                if(arr[i][j] >= 10){
                    arr[i][j] = arr[i][j] % 9;
                }
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("Hourglasses:");
        int result = hourglassSum(arr);

//        bufferedWriter.write(String.valueOf(result));
//        bufferedWriter.newLine();
//
//        bufferedWriter.close();

        scanner.close();
    }
}

