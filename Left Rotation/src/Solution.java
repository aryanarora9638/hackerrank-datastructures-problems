import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int n = 10;
        int d = 4;
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i]=i+1;
        }
        printArray(a);
        int [] output = doRotation(n,d,a);
        printArray(output);
    }

    private static void printArray(int [] output) {
        System.out.print("[");
        for(int item : output){
            System.out.print(item + ",");
        }
        System.out.print("\b]");
        System.out.println();
    }

    private static int[] doRotation(int length, int shift, int[] array) {
        int [] output = new int[array.length];
        Stack<Integer> shiftStack = new Stack<Integer>();
        Stack<Integer> nonShiftStack = new Stack<Integer>();
        for(int i = array.length-1 ; i >= 0 ; i--){
            if(i<shift){
                shiftStack.push(array[i]);
            }
            else{
                nonShiftStack.push(array[i]);
            }
        }
        for(int i = 0; i < output.length; i++){
            if(nonShiftStack.size()!= 0){
                output[i] = nonShiftStack.pop();
            }
            else{
                output[i] = shiftStack.pop();
            }
        }
        return output;
    }
}
