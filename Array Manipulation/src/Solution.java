import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the arrayManipulation function below.
    static long arrayManipulation(int n, int[][] queries) {
        long maxNumber = 0;
        int[] output  = new int[n];

        int startPoint = 0;
        int endPoint = 0;
        int incrementVal = 0;
        int temp = (queries[0].length)-1;
        for (int item = 0; item < queries.length; item++){
            for(int subItem = 0; subItem < queries[0].length; subItem++){
                if(subItem != temp){
                    if(subItem==0){
                        startPoint = queries[item][subItem];
                    }
                    else{// subItem==1
                        endPoint = queries[item][subItem];
                    }
                }
                else {
                    incrementVal = queries[item][subItem];
                }
            }
            for(int start = startPoint-1; start < endPoint; start++) {
                output[start] += incrementVal;
            }
        }


        for(int item : output){
            System.out.println(item);
            if(item > maxNumber){
                maxNumber = item;
            }
            continue;
        }
        return maxNumber;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {


//        int n = 5;
//
//        int m = 3;
//
//        int[][] queries = new int[m][3];
//        for(int item = 0; item < m; item++){
//            for(int subItem = 0; subItem < m; subItem++){
//                queries[item][subItem] = item+subItem+1;
//                System.out.print(queries[item][subItem]);
//            }
//            System.out.println();
//        }

        int n = 10;
        int m = 4;

        int [][] queries = new int[m][3];
        queries[0][0] = 2;
        queries[0][1] = 6;
        queries[0][2] = 8;

        queries[1][0] = 3;
        queries[1][1] = 5;
        queries[1][2] = 7;

        queries[2][0] = 1;
        queries[2][1] = 8;
        queries[2][2] = 1;

        queries[3][0] = 5;
        queries[3][1] = 9;
        queries[3][2] = 15;

        


        long result = arrayManipulation(n, queries);
        System.out.println("Answer is : " + result);

    }
}
