import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the jumpingOnClouds function below.
    static int jumpingOnClouds(int[] c) {
        int totalSteps  = 0;

        for(int step = 0; step < c.length-1; step++){
            if(c[step+1]==0 || c[step+1]==1){
                if(step == c.length-2){
                    if(c[step+1]==0){
                        totalSteps++;
                        continue;
                    }
                }
                if(c[step+2]==0){
                    step++;
                    totalSteps++;
                }
                else {
                    totalSteps++;
                    continue;
                }
            }
        }
        return totalSteps;
    }


    public static void main(String[] args) throws IOException {


        int n = 6;
        int[] c = new int[] {0,0,0,1,0,0};

        int result = jumpingOnClouds(c);
        System.out.println("Result is : " + result);

    }
}
