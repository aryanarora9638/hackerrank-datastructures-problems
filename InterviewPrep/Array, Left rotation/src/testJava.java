import java.util.*;

public class testJava {
    public static void main(String[] args){

        Stack stack = new Stack();
        stack.push(1);
        int output = (int) stack.pop();
        System.out.println(output);

        Queue queue = new LinkedList();
        queue.add(1);
        int output2 = (int) queue.remove();
        System.out.println(output2);

        Deque deque = new LinkedList();
        deque.addFirst(1);
        deque.addLast(2);
        deque.add(3);
        System.out.println(deque.toString());
        deque.remove();
        System.out.println(deque.toString());


        System.out.println("\n ===Iterators=== \n");
        Iterator dequeIterator = deque.iterator();
        while (dequeIterator.hasNext()){
            System.out.println(dequeIterator.next());
        }

        Iterator stackIterator = stack.iterator();
        while (stackIterator.hasNext()){
            System.out.println(stackIterator.next());
        }

        Iterator queueIterator = queue.iterator();
        while (queueIterator.hasNext()){
            System.out.println(queueIterator.next());
        }
    }
}
