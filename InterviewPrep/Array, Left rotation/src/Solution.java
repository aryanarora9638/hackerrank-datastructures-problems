import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;


    public  class Solution {

        // Complete the rotLeft function below.
        static int[] rotLeft(int[] a, int d) {
            int[] outputArray = new int[a.length];

            int numRotations = d ;

            while (numRotations > a.length){
                numRotations = numRotations - a.length;
            }

            Queue q = new LinkedList();
            int tempRot = numRotations;
            int itr = 0;
            while(tempRot>0){
                q.add(a[itr++]);
                tempRot--;
            }

            for(int i = 0; i < (a.length - numRotations); i++){
                outputArray[i] = a[i+itr];
            }
            int filledElements = a.length - numRotations;
            while(q.size()!=0){
                outputArray[filledElements++] = (int) q.remove();
            }

            return outputArray;

        }


        public static void main(String[] args) throws IOException {


            int n = 5;

            int d = 13;

            int[] a = new int[n];

            for (int i = 0; i < n; i++) {
                int aItem = i+1;
                a[i] = aItem;
            }

            int[] result = rotLeft(a, d);

            System.out.print("[");
            for(int output : result){
                System.out.print(output + ",");
            }
            System.out.print("\b");
            System.out.print("]");
        }
    }
