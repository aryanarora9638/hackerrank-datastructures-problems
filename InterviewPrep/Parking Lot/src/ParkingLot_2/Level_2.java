package ParkingLot_2;

import Vehicle.Vehicle;

public class Level_2 {
    final String emptySpot = "empty-spot";
    private int numRowsCols;
    private String[][] spots;
    public Boolean isLevelVisited;
    private int levelNum;

    public Level_2(){
        this.numRowsCols = 10;
        spots = new String[this.numRowsCols][this.numRowsCols];
        initRowsCols();
    }
    public Level_2(int numRowsCols) {
        this.numRowsCols = numRowsCols;
        spots = new String[this.numRowsCols][this.numRowsCols];
        initRowsCols();
    }

    private void initRowsCols() {
        for (int row = 0; row < this.numRowsCols; row++){
            for(int col = 0; col < this.numRowsCols; col++){
                spots[row][col] = emptySpot;
            }
        }
    }

    public boolean park(Vehicle vehicle) {
        this.isLevelVisited = true;

        //try to park in this level
        if(searchSpot(vehicle)){
            return true;
        }

        //if can't park in this level
        else {
            for(Level_2 level_2 : ParkingLot_2.level_2s){
                if(!level_2.isLevelVisited){//if level is not yet visited to find parking spot
                    if(level_2.park(vehicle)){//try to park
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean searchSpot(Vehicle vehicle) {
        int spotRequired = vehicle.getSpotsRequired();

        int row = 0;
        int col;
        while (row < this.numRowsCols){
            col = 0;
            while (col < this.numRowsCols){
                if(spots[row][col] == emptySpot){
                    if(validateSpotsInFront(row,col,spotRequired)){
                        fillSpot(row,col,spotRequired,vehicle.getLicenseNum());
                        generateVehicleReceipt(row,col,vehicle);
                        return true;
                    }
                }
                col++;
            }
            row++;
        }
        return false;
    }


    private boolean validateSpotsInFront(int row, int col, int spotRequired) {
        for(int i = col; i < col+spotRequired; i++){
            if(spots[row][i] != emptySpot){
                return false;
            }
        }
        return true;
    }

    private void fillSpot(int row, int col, int spotRequired, String msg) {
        for (int i = col; i < col+spotRequired; i++){
            spots[row][i] = msg;
        }
    }

    private void generateVehicleReceipt(int row, int col, Vehicle vehicle) {
        this.levelNum = vehicle.hashCode() % this.numRowsCols;
        String receipt = "Level-"+ this.levelNum + " " +"Row-" + row  + " " + "Col-" + col + " to " + (col+(vehicle.getSpotsRequired()-1));
        vehicle.setParkingReceipt(receipt);
    }

    public void unPark(Vehicle vehicle) {
        int row = vehicle.getRowCode();
        int col = vehicle.getColCode();
        int spotsRequired = vehicle.getSpotsRequired();

        for(int spot = col; spot < col+spotsRequired; spot++){
            spots[row][spot] = emptySpot;
        }
        vehicle.setParkingReceipt("unParked");
    }
}
