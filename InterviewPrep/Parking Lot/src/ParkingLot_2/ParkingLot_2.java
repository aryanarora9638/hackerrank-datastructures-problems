package ParkingLot_2;


import ParkingLot.Level;
import Vehicle.Vehicle;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot_2 {
    //number of floors
    final int NUM_LEVEL = 10;
    private int numLevels;
    public static Level_2[] level_2s;
    private Map<Vehicle,String> parkingMap = new HashMap<Vehicle, String>();

    public ParkingLot_2(){
        this.numLevels = NUM_LEVEL;
        level_2s = new Level_2[this.numLevels];
        initLevels(this.numLevels);
    }

    public ParkingLot_2(int numLevels, int numRowsCols){
        this.numLevels = numLevels;
        level_2s = new Level_2[this.numLevels];
        initLevels(numRowsCols);
    }

    private void initLevels(int numRowsCols) {
        for(int level = 0; level < level_2s.length; level++){
            level_2s[level] = new Level_2(numRowsCols);
        }
    }

    public void parkVehicle(Vehicle vehicle){
        int vehicleHashCode = vehicle.hashCode() % this.numLevels;

        if(parkingMap.containsKey(vehicle)){
            System.out.println("Already Parked");
        }
        else {
            if(level_2s[vehicleHashCode].park(vehicle)){
                parkingMap.put(vehicle, vehicle.getParkingReceipt());
                System.out.println("Parked at - " + vehicle.getParkingReceipt());
                makeLevelsUnVisited();
            }
            else {
                System.out.println("Cannot Park");
            }
        }
    }

    private void makeLevelsUnVisited() {
        for(int level = 0; level < level_2s.length; level++){
            level_2s[level].isLevelVisited = false;
        }
    }

    public void unParkVehicle(Vehicle vehicle){

        if(parkingMap.containsKey(vehicle)){
            int vehicleLevelCode = vehicle.getLevelCode();
            parkingMap.remove(vehicle);
            level_2s[vehicleLevelCode].unPark(vehicle);
            System.out.println(vehicle.getParkingReceipt());
        }
        else {
            System.out.println("No Such Vehicle");
        }
    }

}
