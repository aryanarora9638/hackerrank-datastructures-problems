package ParkingLot;

import Vehicle.Vehicle;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    final static int numForRowsAndLevels = 7;
    private Level[] levels;
    private int numLevels;
    private int numLevelRows;
    private Map<Vehicle, String> parkingMap= new HashMap<>();

    public ParkingLot(){
        this.levels = new Level[7];
        this.numLevels = 7;
        this.numLevelRows = 7;
        initializeLevel(numForRowsAndLevels);
    }

    public ParkingLot(int numLevels, int numLevelRow){
        this.numLevels = numLevels;
        this.numLevelRows = numLevelRow;
        this.levels = new Level[numLevels];
        initializeLevel(numLevelRow);
    }

    private void initializeLevel(int numLevelRow) {
        for(int i = 0; i < levels.length; i++){
            levels[i] = new Level(numLevelRow);
        }
    }


    public void parkVehicle(Vehicle vehicle) {
        System.out.println("\n=========================\n");
        //get the level/floor
        int levelCode = vehicle.hashCode() % numLevels;
        levelCode = Math.abs(levelCode);
        //get the row
        int spotCode = vehicle.getLicenseNum().hashCode() % numLevelRows;
        spotCode = Math.abs(spotCode);

        //check is already parked with us
        if(parkingMap.containsKey(vehicle)){
            System.out.println("Vehicle already parked");
            System.out.println(vehicle.getParkingReceipt());
        }

        //try to park in that level and in that row
        System.out.println("Level - " + levelCode + " " + "SpotCode - " + spotCode);
        if(levels[levelCode].park(vehicle, spotCode)){
            parkingMap.put(vehicle,"parked");
            System.out.println("Parked");
            System.out.println(vehicle.getParkingReceipt());
            return;
        }

        while (levelCode+1 < numLevels){
            levelCode+=1;
            System.out.println("-----------------------");
            System.out.println("Searching another Level - " + levelCode);
            if(levels[levelCode].park(vehicle,spotCode)){
                parkingMap.put(vehicle,"parked");
                System.out.println("Parked");
                System.out.println(vehicle.getParkingReceipt());
                return;
            }
        }

        while (levelCode-1 >= 0){
            levelCode-=1;
            System.out.println("-----------------------");
            System.out.println("Searching another Level - " + levelCode);
            if(levels[levelCode].park(vehicle,spotCode)){
                parkingMap.put(vehicle,"parked");
                System.out.println("Parked");
                System.out.println(vehicle.getParkingReceipt());
                return;
            }
        }





        System.out.println("Sorry, vehicle can't be parked as the Lot ran out of Space");

    }

    public void removeVehicle(Vehicle vehicle){
        System.out.println("\n=========================\n");
        if(parkingMap.containsKey(vehicle)){
            System.out.println("Finding Vehicle");
            System.out.println(vehicle.getParkingReceipt());

            //get the level/floor
            int levelCode = vehicle.hashCode() % numLevels;
            levelCode = Math.abs(levelCode);
            //get the row
            int spotCode = vehicle.getLicenseNum().hashCode() % numLevelRows;
            spotCode = Math.abs(spotCode);

            System.out.println("Level - " + levelCode + " " + "SpotCode - " + spotCode);
            //remove vehicle from the level
            if(levels[levelCode].unPark(vehicle,spotCode)){
                parkingMap.remove(vehicle);
                System.out.println("Unparked");
            }
        }
        else {
            System.out.println("No Such Vehicle in the Lot");
        }
    }
}
