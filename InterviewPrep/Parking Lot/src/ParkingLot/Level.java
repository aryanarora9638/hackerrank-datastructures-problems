package ParkingLot;

import Vehicle.Vehicle;

public class Level {
    final static String emptySpot = "empty";
    private String[][] spots;
    private int numRow;
    private String isAvailable = "-";

    public Level(){
        spots = new String[7][7];
        putValueToSpot(emptySpot);
    }

    public Level(int numRow){
        this.numRow = numRow;
        spots = new String[numRow][numRow];
        putValueToSpot(emptySpot);
    }

    private void putValueToSpot(String spotStatus) {
        for(int row = 0; row < spots.length; row++){
            for(int col = 0; col < spots.length; col++){
                spots[row][col] = spotStatus;
            }
        }
    }


    public boolean park(Vehicle vehicle, int spotCode) {
        int spotsRequired = vehicle.getSpotsRequired();
        int rowCode = spotCode;
        boolean parked = false;

        if(searchRow(rowCode,spotsRequired,vehicle)){
            parked = true;
        }

        int temp = 0;
        if(!parked){
            while (rowCode+temp < numRow){
                System.out.println("alt search at row(after) : " + rowCode + "+" + temp + "=" +(rowCode+temp));
                if(searchRow(rowCode+temp,spotsRequired,vehicle)){
                    parked = true;
                    break;
                }
                temp++;
            }
        }

        temp = 1;
        if(!parked){
            while (rowCode-temp >= 0){
                System.out.println("alt search at row(before): " + rowCode + "-" + temp + "=" + (rowCode-temp));
                if(searchRow(rowCode-temp,spotsRequired,vehicle)){
                    parked = true;
                    break;
                }
                temp++;
            }
        }

        return parked;
    }

    private boolean searchRow(int rowCode, int spotsRequired, Vehicle vehicle) {
        //search all the cols for space in the given row
        for(int col = 0; col < spots[rowCode].length; col++){
            String spot = spots[rowCode][col];
            //if spot is empty
            if(spot.equalsIgnoreCase(emptySpot)){
                this.isAvailable = emptySpot;
                //find spots nearby to this spot to fit the vehicle
                if(lookUpSpots(rowCode, col, spotsRequired)){
                    fillSpot(rowCode,col,vehicle,spotsRequired);
                    return true;
                }
            }
        }
        return false;
    }

    private void fillSpot(int rowCode, int col, Vehicle vehicle, int spotRequired) {
        int colParkedStartIndex = 0;
        if(isAvailable.equalsIgnoreCase("before")){
            for(int i = col; i > col-spotRequired; i--){
                spots[rowCode][i] = vehicle.getLicenseNum();
            }
            colParkedStartIndex = col-(spotRequired-1);
        }
        else{//available after
            colParkedStartIndex = col;
            for(int i = col; i < col+spotRequired; i++){
                spots[rowCode][i] = vehicle.getLicenseNum();
            }
        }
        vehicle.setParkingReceipt(vehicle.getParkingReceipt().concat("Row-"+rowCode +" "+
                "Col-"+colParkedStartIndex+" "));

    }

    private boolean lookUpSpots(int row,int col, int spotsRequired) {
        boolean isAvailableAfter = true;
        boolean isAvailableBefore = true;

        if(spotsRequired>1){
            if(col+(spotsRequired-1) >= numRow || col-(spotsRequired-1) < 0){
                //will not fit
                return false;
            }
        }

        //before
        for(int i = col; i > i-spotsRequired && i >= 0; i--){
            if(spots[row][i] != emptySpot){
                isAvailableBefore = false;
            }
//            if(i == 0){
//                break;
//            }
        }

        //first check for before spots, if not found, check for after
        //if still not found, no space in the row, return false
        if(isAvailableBefore){
            this.isAvailable = "before";
            return true;
        }


        //after
        for(int i = col; i < i+spotsRequired && i < numRow; i++){
            if(spots[row][i] != emptySpot) {
                isAvailableAfter = false;
            }
//            if(i == numRow-1){
//                break;
//            }
        }

        if(isAvailableAfter){
            this.isAvailable = "after";
            return true;
        }
        else {
            return false;
        }

    }

    public boolean unPark(Vehicle vehicle, int spotCode) {
        //search ideally where the vehicle shud be based on it's receipt
        int rowCode = vehicle.getParkingReceipt().indexOf("-");
        int colCode = vehicle.getParkingReceipt().indexOf("-",rowCode+1);

        rowCode =  Integer.parseInt(vehicle.getParkingReceipt().substring(rowCode+1,rowCode+2));
        colCode = Integer.parseInt(vehicle.getParkingReceipt().substring(colCode+1,colCode+2));

        System.out.println("Expected Location of Vehicle " + rowCode + "," + colCode);

        if(spots[rowCode][colCode].equalsIgnoreCase(vehicle.getLicenseNum())){
            System.out.println("Vehicle Found");
            System.out.println("Removing Vehicle - ");
            for(int i = colCode; i < colCode+vehicle.getSpotsRequired(); i++){
                spots[rowCode][i] = emptySpot;
            }
            System.out.println("Vehicle Unparked");
            return true;
        }
        else {
            System.out.println("Vehicle not at expected Location");
            for(int row = 0; row < spots.length; row++){
                for(int col = 0; col < spots[row].length; col++){
                    if(spots[row][col].equalsIgnoreCase(vehicle.getLicenseNum())){
                        System.out.println("Vehicle Found");
                        System.out.println("Removing Vehicle - ");
                        for(int i = col; i < col+vehicle.getSpotsRequired(); i++){
                            spots[row][i] = emptySpot;
                        }
                        System.out.println("Vehicle Unparked");
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
