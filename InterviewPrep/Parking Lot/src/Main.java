import ParkingLot.ParkingLot;
import ParkingLot_2.ParkingLot_2;
import Vehicle.Vehicle;
import Vehicle.Car;
import Vehicle.Bus;
import Vehicle.Bike;


public class Main {
   public static Vehicle[] vehicles = new Vehicle[1000];
    public static void main(String[] args){
        //M-1
//        ParkingLot parkingLot = new ParkingLot();
//        int i = 0;
//        for(Vehicle vehicle : vehicles){
//            vehicle = new Car("ABAB-357", "BLUE");
//            vehicle.setLicenseNum(vehicle.getLicenseNum().concat(String.valueOf(i++)));
//            parkingLot.parkVehicle(vehicle);
//        }


        //M-2 - better and faster
        ParkingLot_2 parkingLot_2 = new ParkingLot_2();

        int i = 0;
        for(Vehicle vehicle : vehicles){
            vehicle = new Bike("ABAB-357", "Blue");
            vehicle.setLicenseNum(vehicle.getLicenseNum().concat(String.valueOf(i++)));
            parkingLot_2.parkVehicle(vehicle);
            parkingLot_2.unParkVehicle(vehicle);
        }
    }
}
