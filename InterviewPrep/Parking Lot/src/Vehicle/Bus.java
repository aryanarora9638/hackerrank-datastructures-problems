package Vehicle;

public class Bus extends Vehicle{
    public Bus(){
        super();
    }

    public Bus(String licenseNum, String color){
        super("Bus",5,licenseNum,color);
    }
}
