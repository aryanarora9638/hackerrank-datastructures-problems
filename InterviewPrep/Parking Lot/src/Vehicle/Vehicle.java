package Vehicle;

public class Vehicle {
    private String typeOfVehicle;
    private int spotsRequired;
    private String licenseNum;
    private String color;
    private String parkingReceipt;

    public Vehicle(){
        this.typeOfVehicle = "-";
        this.spotsRequired = -1;
        this.licenseNum = "-";
        this.color = "-";
        this.parkingReceipt = "-";
    }

    public Vehicle(String typeOfVehicle, int spotsRequired, String licenseNum, String color){
        this.typeOfVehicle = typeOfVehicle;
        this.color = color;
        this.licenseNum = licenseNum;
        this.spotsRequired = spotsRequired;
        this.parkingReceipt = "-";
        makeParkingReceipt();
    }

    private void makeParkingReceipt() {
        String receipt = "Parked at : ";
        setParkingReceipt(receipt);
    }

    public String getTypeOfVehicle() {
        return typeOfVehicle;
    }

    public void setTypeOfVehicle(String typeOfVehicle) {
        this.typeOfVehicle = typeOfVehicle;
    }

    public int getSpotsRequired() {
        return spotsRequired;
    }

    public void setSpotsRequired(int spotsRequired) {
        this.spotsRequired = spotsRequired;
    }

    public String getLicenseNum() {
        return licenseNum;
    }

    public void setLicenseNum(String licenseNum) {
        this.licenseNum = licenseNum;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getParkingReceipt() {
        return parkingReceipt;
    }

    public void setParkingReceipt(String parkingReceipt) {
        this.parkingReceipt = parkingReceipt;
    }

    public int getLevelCode(){
        int startIndex = this.getParkingReceipt().indexOf("-")+1;
        int endIndex = startIndex+1;
        int vehicleLevelCode = Integer.parseInt(this.getParkingReceipt().substring(startIndex,endIndex));

        return vehicleLevelCode;
    }

    public int getRowCode(){
        int startIndex = this.getParkingReceipt().indexOf("-", this.getParkingReceipt().indexOf("Row-")) +1;
        int endIndex = startIndex+1;
        int rowCode = Integer.parseInt(this.getParkingReceipt().substring(startIndex,endIndex));

        return rowCode;

    }

    public int getColCode(){
        int startIndex = this.getParkingReceipt().indexOf("-", this.getParkingReceipt().indexOf("Col-")) +1;
        int endIndex = startIndex+1;
        int colCode = Integer.parseInt(this.getParkingReceipt().substring(startIndex,endIndex));

        return colCode;

    }
}
