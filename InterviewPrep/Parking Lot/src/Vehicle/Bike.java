package Vehicle;

public class Bike extends Vehicle{
    public Bike(){
        super();
    }

    public Bike(String licenseNum, String color){
        super("Bike",1,licenseNum,color);
    }
}
