package Vehicle;

public class Car extends Vehicle {

    public Car(){
        super();
    }

    public Car(String licenseNum, String color){
        super("Car",2,licenseNum,color);
    }

}
