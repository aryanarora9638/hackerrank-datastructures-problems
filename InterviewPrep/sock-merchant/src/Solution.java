import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;
import java.util.Arrays;

public class Solution {

    // Complete the sockMerchant function below.
    static int sockMerchant(int n, int[] ar) {
        //METHOD:2
        Arrays.sort(ar);
        System.out.println(Arrays.toString(ar));
        int pairOutput = 0;
        for(int i = 0; i < ar.length-1; i++){
            if(ar[i] == ar[i+1]){
                pairOutput++;
                i += 1;
            }
            else {
                continue;
            }
        }
        return pairOutput;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {


        int n = 10;

        //int[] ar = new int[n];
//        for (int i = 0; i < n; i++) {
//            ar[i] = i;
//            ar[i+1] = i;
//            i+=1;
//        }
//        ar[ar.length-1] = 9;

        int [] ar = {10, 20, 20, 10, 10, 30, 50, 10, 20};





        int result = sockMerchant(n, ar);
        System.out.println("Result is: " + result);

    }
}
