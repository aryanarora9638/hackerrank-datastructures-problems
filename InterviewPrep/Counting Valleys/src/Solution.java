import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    public static boolean isUphill = false;

    // Complete the countingValleys function below.
    static int countingValleys(int n, String s) {
        int level = 0;
        int up = 1;
        int down = 1;

        int valleyOutput = 0;

        for(int step = 0; step < s.length(); step++) {
            if (s.charAt(step) == 'U') {
                level += up;
                if(level == 0){
                    valleyOutput++;
                }
            }
            else {
                level -= down;
            }
        }

        return valleyOutput;
    }



    public static void main(String[] args) throws IOException {


        int n = 8;
        String s = "UDDDUDUU";
        int result = countingValleys(n, s);
        System.out.println("Result is : " + result);


    }
}
