import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the repeatedString function below.
    static long repeatedString(String s, long n) {
        //M-1
//        long totalRep = 0;
//        String tempInput = "";
//        for (int i = 0; i < n; i++){
//            tempInput+=s;
//        }
//        tempInput = tempInput.substring(0, (int) n).trim();
//
//        for (int i = 0; i < n; i++){
//            if(tempInput.charAt(i)=='a'){
//                totalRep++;
//            }
//        }
//        return totalRep;


        //M-2
        long totalRep = 0;
        int subInString = 0;
        for(char x : s.toCharArray()){
            if(x=='a'){
                subInString++;
            }
        }
        long extra =  (n % s.length());
        long ratio =  (n / s.length());
        int temp = 0;
        for (int i = 0 ; i < extra; i++){
           if(s.toCharArray()[i] == 'a'){
               temp++;
           }
        }
        totalRep = (subInString*ratio) + temp;

        return totalRep;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        long n = 1000000000000;
        String s = "aba";
        long result = repeatedString(s, n);
        System.out.println("Result is :  " + result);
    }
}