import java.util.*;

public class Solution {
    public static void oddNumFinder(int[] arr){
        Map<Integer, Integer> map = new HashMap<>();
        for(int item : arr){
            if(map.containsKey(item)){
                Integer newValue = map.get(item);
                map.put(item, newValue+1);
            }
            else {
                map.put(item, 1);
            }
        }


        Iterator iterator = map.keySet().iterator();
        while (iterator.hasNext()){
            Integer key = (Integer) iterator.next();
            if(map.get(key) %2 == 1){
                System.out.println("\n\nodd - " + key);
            }
        }
    }
    public static void main(String[] args){
        //input
        int [] ar1 = new int[30];
        int val = 0;
        for(int i = 0; i < ar1.length-1; i++){
            ar1[i] = ar1[i+1] = val++;
            i++;
        }
        int temp = 7;
        ar1[13] = temp;

        System.out.print("[");
        for(int item : ar1){
            System.out.print(item +",");
        }
        System.out.print("\b]");

        oddNumFinder(ar1);
    }
}
